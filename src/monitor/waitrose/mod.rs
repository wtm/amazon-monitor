use reqwest::header::{HeaderMap, HeaderName};
use reqwest::Client;
use super::{CheckResult, Checker};
use log::{error, info, debug};
use std::pin::Pin;
use std::future::Future;

const URL: &str = "https://www.waitrose.com/api/graphql-prod/graph/live";
const GRAPHQL_QUERY: &str = include_str!("./query.graphql");
const GRAPHQL_QUERY_GENERATE_SESSION: &str = include_str!("./gen-session.graphql");

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[allow(non_snake_case)]
pub struct QueryParams {
	branchId: String,
	addressId: String,
}

#[derive(Serialize, Clone, Debug)]
struct PostBody<Vars: Serialize> {
	query: &'static str,
	variables: Vars
}

#[derive(Serialize, Clone, Debug)]
#[allow(non_snake_case)]
struct PostVarsQuery {
	slotDaysInput: PostVarsSlotDaysInput
}

#[derive(Serialize, Clone, Debug)]
struct PostVarsGenerateSession<'a> {
	session: PostVarsSessionParam<'a>
}

#[derive(Serialize, Clone, Debug)]
#[allow(non_snake_case)]
struct PostVarsSessionParam<'a> {
	clientId: &'static str,
	customerId: &'static str,
	username: &'a str,
	password: &'a str
}

impl<'a> PostVarsGenerateSession<'a> {
	fn get(username: &'a str, password: &'a str) -> Self {
		Self {
			session: PostVarsSessionParam {
				clientId: "WEB_APP",
				customerId: "-1",
				username, password
			}
		}
	}
}

#[derive(Serialize, Clone, Debug)]
#[allow(non_snake_case)]
struct PostVarsSlotDaysInput {
	#[serde(flatten)]
	user_params: QueryParams,

	slotType: &'static str,
	fromDate: String,
	size: usize,
	customerOrderId: String
}

const SLOTTYPE: &str = "DELIVERY";
const NOT_AVAILABLE_STATUS: &[&str] = &["FULLY_BOOKED", "UNAVAILABLE"];

use crate::cookiejar::CookieJar;

pub struct WaitroseChecker {
	headers: HeaderMap,
	cookies: CookieJar,
	query_params: QueryParams,
	authorization: Option<String>,
	order_id: Option<String>,
	username: String,
	password: String
}

pub fn get_checker(headers: HeaderMap, query_params: QueryParams, username: String, password: String) -> WaitroseChecker {
	let (cookies, mut headers) = CookieJar::extract_from_headers(headers);
	while headers.remove(HeaderName::from_static("authorization")).is_some() {}
	WaitroseChecker {
		headers, cookies, query_params, authorization: None, order_id: None,
		username, password
	}
}

impl WaitroseChecker {
	async fn update_credentials(&mut self, c: &Client) -> Result<(), String> {
		info!("Updating waitrose session");
		let resp = c.post(URL)
			.headers(self.headers.clone())
			.header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
			.header(HeaderName::from_static("authorization"), "Bearer unauthenticated")
			.json(&{
				PostBody {
					query: GRAPHQL_QUERY_GENERATE_SESSION,
					variables: PostVarsGenerateSession::get(&self.username, &self.password)
				}
			}).send().await.map_err(|e| format!("{}", e))?;
			use serde_json::Value;
			let json: Value = resp.json().await.map_err(|e| format!("Unable to parse response: {}", e))?;
			let data = json.as_object()
				.and_then(|o| o.get("data"))
				.and_then(|o| o.as_object())
				.and_then(|o| o.get("generateSession"))
				.and_then(|o| o.as_object());
			if data.is_none() {
				return Err(format!("Invalid response: {:?}", &json));
			}
			let data = data.unwrap();
			if let Some(failure_msg) =
				data.get("failures")
					.and_then(|o| o.as_object())
					.and_then(|o| o.get("message"))
					.and_then(|o| o.as_str()) {
						return Err(format!("Server returned failure: {}", failure_msg));
					}
			let token = data.get("token").and_then(|o| o.as_str());
			let order_id = data.get("order_id").and_then(|o| o.as_str());
			if token.is_none() || order_id.is_none() {
				return Err(format!("Server did not give use token / order_id: {:?}", &data));
			}
			self.authorization = Some(token.unwrap().to_owned());
			self.order_id = Some(order_id.unwrap().to_owned());

			Ok(())
	}

	async fn _check(&mut self, c: &Client) -> Result<CheckResult, String> {
		loop {
			if self.authorization.is_none() || self.order_id.is_none() {
				self.update_credentials(c).await?;
				continue;
			}
			use crate::YMD;
			use chrono::{NaiveDate, DateTime};
			let today = DateTime::from(std::time::SystemTime::now()).date();
			let query_body = PostBody {
				query: GRAPHQL_QUERY,
				variables: PostVarsQuery {
					slotDaysInput: PostVarsSlotDaysInput {
						user_params: self.query_params.clone(),
						customerOrderId: self.order_id.clone().unwrap(),
						slotType: SLOTTYPE,
						fromDate: format!("{}", today.format(YMD)),
						size: 7
					}
				}
			};
			let resp = c.post(URL)
				.headers(self.headers.clone())
				.header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
				.header(HeaderName::from_static("authorization"), format!("Bearer {}", self.authorization.as_ref().unwrap()))
				.json(&query_body)
				.send().await.map_err(|e| format!("{}", e))?;
			self.cookies.apply(&resp);
			if resp.status().as_u16() == 403 {
				debug!("Got 403.");
				self.update_credentials(c).await?;
				continue;
			}
			use serde_json::Value;
			let text = resp.text().await.map_err(|e| format!("Unable to get response text: {}", e))?;
			let json: Value = serde_json::from_str(&text).map_err(|e| format!("Unable to parse JSON: {}", e))?;
			if json.as_object().and_then(|o| o.get("errors")).is_some() {
				debug!("Got {}. Trying refresh token.", &text);
				self.update_credentials(c).await?;
				continue;
			}

			#[allow(non_snake_case)]
			let EINVALID = "Invalid response".to_owned();
			const EIGNORE: &str = "Invalid body. Ignoring this day.";
			let slot_days = json.as_object()
				.and_then(|o| o.get("data"))
				.and_then(|o| o.as_object())
				.and_then(|o| o.get("slotDays"))
				.and_then(|o| o.as_object()).ok_or(format!("Invalid response: {}", &text))?;
			if !slot_days.get("failures").map(|o| o.is_null()).unwrap_or(true) {
				return Err(format!("Server returned failure: {:?}", slot_days.get("failures").unwrap()));
			}
			let days = slot_days.get("content").and_then(|o| o.as_array());
			if days.is_none() {
				return Err(EINVALID);
			}
			let days = days.unwrap();
			let mut n = 0usize;
			let mut ret: Option<NaiveDate> = None;
			for d in days {
				let day = d.as_object()
					.and_then(|o| o.get("date"))
					.and_then(|d| d.as_str());
				if day.is_none() {
					error!("{}", EIGNORE);
					continue;
				}
				let day = match NaiveDate::parse_from_str(day.unwrap(), YMD) {
					Ok(d) => d,
					Err(_) => {
						error!("Invalid date string: {}", day.unwrap());
						continue;
					}
				};
				let slots = match d.as_object()
														.and_then(|o| o.get("slots"))
														.and_then(|o| o.as_array()) {
					Some(slots) => slots,
					None => {
						error!("{}", EIGNORE);
						continue;
					}
				};
				let statuss: Vec<Option<&str>> = slots.iter().map(|o| {
					o.as_object()
						.and_then(|o| o.get("status"))
						.and_then(|o| o.as_str())
				}).collect();
				let has_available = statuss.iter().any(|s| {
					if s.is_none() {
						return false;
					}
					let status = s.unwrap();
					NOT_AVAILABLE_STATUS.iter().copied().find(|x| *x == status).is_none()
				});
				if has_available {
					match ret {
						Some(ref prev_day) => {
							if prev_day > &day {
								ret = Some(day);
							}
						},
						None => {
							ret = Some(day);
						}
					};
				} else if statuss.contains(&None) {
					error!("Day {} seems wrong.", &day);
					continue;
				}
				n += 1;
			}

			break match ret {
				None => {
					if n < 6 {
						Err("Data seems wrong".to_owned())
					} else {
						Ok(CheckResult::NoSlots)
					}
				},
				Some(day) => {
					Ok(CheckResult::SlotsOn(day))
				}
			};
		}
	}
}

impl Checker for WaitroseChecker {
	fn name(&self) -> &'static str {
		"Waitrose"
	}
	fn check<'s>(&'s mut self, c: &'s Client) -> Pin<Box<dyn Future<Output = Result<CheckResult, String>> + 's>> {
		Box::pin(self._check(c))
	}
}
