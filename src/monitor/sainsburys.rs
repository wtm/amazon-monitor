use super::{Checker, CheckResult};
use crate::cookiejar::CookieJar;
use reqwest::header::{HeaderName, HeaderMap};
use reqwest::Client;
use std::pin::Pin;
use std::future::Future;

pub struct SainsburysChecker {
  cookies: CookieJar,
  headers: HeaderMap
}

pub fn get_checker(headers: HeaderMap) -> SainsburysChecker {
  let (cookies, headers) = CookieJar::extract_from_headers(headers);
  SainsburysChecker {
    cookies, headers
  }
}

impl SainsburysChecker {
  async fn _check(&mut self, c: &Client) -> Result<CheckResult, String> {
    let res = c.get("https://www.sainsburys.co.uk/shop/BookingDeliverySlotDisplayView?currentPageUrl=https%3A%2F%2Fwww.sainsburys.co.uk%2Fwebapp%2Fwcs%2Fstores%2Fservlet%2FAjaxOrderItemDisplayView%3FlangId%3D44%26storeId%3D10151&catalogId=12975&previousPageName=PostCodeSuccess&langId=44&storeId=10151&clickAndCollect=false")
      .headers(self.headers.clone())
      .header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
      .header(HeaderName::from_static("referer"), "https://www.sainsburys.co.uk/shop/PostCodeCheckSuccessView?currentPageUrl=https%3A%2F%2Fwww.sainsburys.co.uk%2Fwebapp%2Fwcs%2Fstores%2Fservlet%2FAjaxOrderItemDisplayView%3FlangId%3D44%26storeId%3D10151&catalogId=12975&langId=44&storeId=10151&goodNewsPage=false")
      .send().await.map_err(|e| format!("Request: {}", e))?;
    self.cookies.apply(&res);
    if res.status().is_redirection()
      && res.headers().get(HeaderName::from_static("location"))
            .ok_or_else(|| "Got redirection, but no location header")?.to_str().map_err(|e| format!("{}", e))?
            .starts_with("/groceries-api/visitor-priority/redirect") {
      return Ok(CheckResult::NoSlots);
    }
    if res.status().is_client_error() || res.status().is_server_error() {
      return Err(format!("Got {}", res.status().as_u16()));
    }
    Ok(CheckResult::SlotsAvailable)
  }
}

impl Checker for SainsburysChecker {
  fn name(&self) -> &'static str {
    "Sainsburys"
  }

  fn check<'s>(&'s mut self, c: &'s Client) -> Pin<Box<dyn Future<Output = Result<CheckResult, String>> + 's>> {
    Box::pin(self._check(c))
  }
}
