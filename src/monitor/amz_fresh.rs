use reqwest::header::{HeaderMap, HeaderName};
use reqwest::Client;
use super::{CheckResult, Checker};
use log::{error, info};
use std::pin::Pin;
use std::future::Future;
use crate::cookiejar::CookieJar;

const URL: &str = "https://www.amazon.co.uk/afx/slotselection/";

pub struct AmzChecker {
	headers: HeaderMap,
	cookies: CookieJar
}

pub fn get_checker(headers: HeaderMap) -> AmzChecker {
	let (cookies, headers) = CookieJar::extract_from_headers(headers);
	AmzChecker{
		headers,
		cookies
	}
}

impl AmzChecker {
	async fn _check(&mut self, c: &Client) -> Result<CheckResult, String> {
		let resp =  c.get(URL)
			.headers(self.headers.clone())
			.header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
			.send().await.map_err(|e| format!("{}", e))?;
		self.cookies.apply(&resp);
		let html = resp.text().await.map_err(|e| format!("Unable to parse UTF-8: {}", e))?;

		use select::document::Document;
		use select::predicate::Class;
		let doc = Document::from(&html[..]);
		let mut ret = None;
		let mut n = 0usize;
		for day_container in doc.find(Class("Date-slot-container")) {
			let id = day_container.attr("id").unwrap_or("");
			const ID_BEGIN: &str = "slot-container-";
			if !id.starts_with(ID_BEGIN) {
				continue;
			}
			let date_str = &id[ID_BEGIN.len()..];
			use chrono::NaiveDate;
			use crate::YMD;
			let day = match NaiveDate::parse_from_str(date_str, YMD) {
				Ok(d) => d,
				Err(e) => {
					error!("Invalid date on id attr: {} ({})", date_str, e);
					continue;
				}
			};
			let alltext = day_container.text();
			const TIME_OF_DAY: &[&str] = &["morning", "afternoon", "evening"];
			for t in TIME_OF_DAY.iter() {
				let t = *t;
				let search_str = format!("No {} delivery windows are available for ", t);
				if alltext.find(&search_str).is_none() {
					info!("{} slot available on {}!", t, date_str);
					if ret.is_none() || ret.unwrap() > day {
						ret = Some(day);
					}
				}
			}
			n += 1;
		}
		Ok(match ret {
			Some(day) => CheckResult::SlotsOn(day),
			None => {
				if n < 3 {
					return Err("Page seems wrong!".to_owned());
				}
				CheckResult::NoSlots
			}
		})
	}
}

impl Checker for AmzChecker {
	fn name(&self) -> &'static str {
		"Amazon Fresh"
	}
	fn check<'s>(&'s mut self, c: &'s Client) -> Pin<Box<dyn Future<Output = Result<CheckResult, String>> + 's>> {
		Box::pin(self._check(c))
	}
}
