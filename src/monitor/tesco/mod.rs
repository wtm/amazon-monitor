use crate::cookiejar::CookieJar;
use reqwest::header::{HeaderMap, HeaderName};
use super::{Checker, CheckResult};
use reqwest::Client;
use std::future::Future;
use std::pin::Pin;
use chrono::{DateTime, NaiveDate, NaiveDateTime};
use crate::YMD;
use std::time::SystemTime;
use log::{error, info, debug};
use serde::Serialize;

pub struct TescoChecker {
	headers: HeaderMap,
	cookies: CookieJar,
	username: String,
	password: String
}

pub fn get_checker(headers: HeaderMap, username: String, password: String) -> TescoChecker {
	let (cookies, headers) = CookieJar::extract_from_headers(headers);
	TescoChecker{
		headers,
		cookies,
		username,
		password
	}
}

impl TescoChecker {
	async fn _check(&mut self, c: &Client) -> Result<CheckResult, String> {
		'retry_loop: loop {
			let today = DateTime::from(SystemTime::now()).date();
			let mut ret: Option<NaiveDate> = None;
			let mut has_invalid = false;
			for sg in [1, 4].iter().copied() {
				let url = format!("https://www.tesco.com/groceries/en-GB/slots/delivery/{}?slotGroup={}", today.format(YMD), sg);
				use std::str::FromStr;
				let resp =  c.get(&url)
					.headers(self.headers.clone())
					.header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
					.header(HeaderName::from_str("X-Requested-With").unwrap(), "XMLHttpRequest")
					.header(HeaderName::from_str("ADRUM").unwrap(), "isAjax:true")
					.header(HeaderName::from_static("referer"), "https://www.tesco.com/groceries/en-GB/slots/delivery")
					.header(HeaderName::from_static("origin"), "https://www.tesco.com")
					.header(HeaderName::from_static("accept"), "application/json")
					.send().await.map_err(|e| format!("{}", e))?;
				self.cookies.apply(&resp);
				if resp.status().is_redirection() {
					self.re_login(c).await?;
					continue 'retry_loop;
				}
				let resp_text = resp.text().await.map_err(|e| format!("Unable to get response text: {}", e))?;
				use serde_json::Value;
				let json: Value = serde_json::from_str(&resp_text[..]).map_err(|e| format!("Parsing json response: {}", e))?;
				let slots = json.as_object()
					.and_then(|o| o.get("slots"))
					.and_then(|o| o.as_array());
				if slots.is_none() {
					if let Some(_probe_url) = json.as_object()
								.and_then(|o| o.get("redirect-to"))
								.and_then(|o| o.as_str()) {
						self.re_login(c).await?;
						continue 'retry_loop;
					}
					return Err(format!("Response invalid: {}.", &serde_json::to_string_pretty(&json).unwrap()));
				}
				let slots = slots.unwrap();
				for slot in slots {
					let status = slot.as_object().and_then(|o| o.get("status")).and_then(|o| o.as_str());
					if status.is_none() {
						has_invalid = true;
					}
					let status = status.unwrap();
					if !status.eq_ignore_ascii_case("unavailable") {
						info!("Slot found with status {}!", status);
						let mut start = slot.as_object()
							.and_then(|o| o.get("start"))
							.and_then(|o| o.as_str())
							.and_then(|o| NaiveDateTime::from_str(o.trim_end_matches('Z')).ok());
						if start.is_none() {
							error!("Slot exist, but can't parse start date.");
							start = Some(DateTime::from(SystemTime::now()).naive_local());
						}
						let start = start.unwrap();
						let day = start.date();
						if ret.is_none() || ret.unwrap() > day {
							ret = Some(day);
						}
					}
				}
			}

			break match ret {
				Some(day) => Ok(CheckResult::SlotsOn(day)),
				None => {
					if !has_invalid {
						Ok(CheckResult::NoSlots)
					} else {
						Err("Some response invalid.".to_owned())
					}
				}
			};
		}
	}

	async fn re_login(&mut self, c: &Client) -> Result<(), String> {
		info!("Trying to re-login to Tesco");

		self.cookies.0.clear();

		let resp = c.get("https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F")
			.headers(self.headers.clone())
			.header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
				.header(HeaderName::from_static("accept"), "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
			.send().await.map_err(|e| format!("getting login page: {}", e))?;
		self.cookies.apply(&resp);
		if resp.status().is_redirection() {
			debug!("Login page redirected - login not needed.");
			return Ok(());
		} else if !resp.status().is_success() {
			return Err(format!("Login page returned {}", resp.status().as_u16()));
		}
		let login_page_html = resp.text().await.map_err(|e| format!("getting html: {}", e))?;

		use select::document::Document;
		use select::predicate::Attr;
		let doc = Document::from(&login_page_html[..]);
		let csrf_element = doc.find(Attr("id", "_csrf")).next().ok_or_else(|| "Can't find csrf token in html".to_owned())?;
		let csrf_token = csrf_element.attr("value").ok_or_else(|| "_csrf input has no value".to_owned())?;
		let state = doc.find(Attr("id", "state")).next().ok_or_else(|| "Can't find [id=state]".to_owned())?
			.attr("value").ok_or_else(|| "[id=state] has no value".to_owned())?;

		use std::str::FromStr;

		// curl 'https://secure.tesco.com/account/confirm?without' -H 'User-Agent:
		// ...' -H 'Accept: */*' -H 'Accept-Language: ...' --compressed -H 'Referer:
		// https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F'
		// -H 'ADRUM: isAjax:true' -H 'DNT: 1' -H 'Connection: keep-alive' -H
		// 'Cookie: ...' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache'
		let _ = c.get("https://secure.tesco.com/account/confirm?without")
			.headers(self.headers.clone())
			.header(HeaderName::from_static("accept"), "*/*")
			.header(HeaderName::from_static("referer"), "https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F")
			.header(HeaderName::from_str("ADRUM").unwrap(), "isAjax:true")
			.header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
			.send().await.map_err(|e| error!("Aux request: {}", e)).map(|resp| {
				if !resp.status().is_success() {
					error!("Aux request /account/confirm: {}", resp.status().as_u16());
				}
				self.cookies.apply(&resp);
			});

		// curl 'https://secure.tesco.com/account/en-GB/update-fingerprint' -H
		// 'User-Agent: ...' -H 'Accept: application/json' -H 'Accept-Language: ...'
		// --compressed -H 'Referer:
		// https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F'
		// -H 'Content-Type: application/json' -H 'CSRF-token: ...' -H 'Origin:
		// https://secure.tesco.com' -H 'DNT: 1' -H 'Connection: keep-alive' -H
		// 'Cookie: ...' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data
		// ...
		let _ = c.post("https://secure.tesco.com/account/en-GB/update-fingerprint")
			.headers(self.headers.clone())
			.header(HeaderName::from_static("accept"), "application/json")
			.header(HeaderName::from_static("content-type"), "application/json")
			.header(HeaderName::from_static("referer"), "https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F")
			.header(HeaderName::from_str("CSRF-token").unwrap(), csrf_token)
			.header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
			.body(include_str!("./fingerprint.json").replace("___STATE___", state))
			.send().await.map_err(|e| error!("Aux request: {}", e)).map(|resp| {
				if !resp.status().is_success() {
					error!("Aux request /account/en-GB/update-fingerprint: {}", resp.status().as_u16());
				}
			});

		// curl 'https://secure.tesco.com/assets/70e1e3c71945aca3e98bb20a7cf1' -H
		// 'User-Agent: ...' -H 'Accept: */*' -H 'Accept-Language: ...' --compressed
		// -H 'Referer:
		// https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F'
		// -H 'ADRUM: isAjax:true' -H 'Content-Type: text/plain;charset=UTF-8' -H
		// 'Origin: https://secure.tesco.com' -H 'Cookie: ...' -H 'Pragma: no-cache'
		// -H 'Cache-Control: no-cache' --data '...'
		let sensor_resp = c.post("https://secure.tesco.com/assets/70e1e3c71945aca3e98bb20a7cf1")
			.headers(self.headers.clone())
			.header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
			.header(HeaderName::from_static("accept"), "*/*")
			.header(HeaderName::from_static("referer"), "https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F")
			.header(HeaderName::from_str("ADRUM").unwrap(), "isAjax:true")
			.header(HeaderName::from_static("content-type"), "text/plain;charset=UTF-8")
			.header(HeaderName::from_static("origin"), "https://secure.tesco.com")
			.body(include_str!("./sensor_data.json"))
			.send().await.map_err(|e| format!("Unable to post sensor data: {}", e));
		if let Err(e) = sensor_resp {
			error!("{}", e);
		} else {
			let resp = sensor_resp.unwrap();
			self.cookies.apply(&resp);
			debug!("sensor response: {}", resp.text().await.unwrap_or_else(|_| "???".to_owned()));
		}

		#[derive(Serialize)]
		struct PostBody<'a> {
			username: &'a str,
			password: &'a str,
			state: &'a str,
			_csrf: &'a str
		}

		let body = PostBody {
			username: &self.username,
			password: &self.password,
			state: &state,
			_csrf: &csrf_token
		};
		let body_text = serde_urlencoded::to_string(body).unwrap();

		let resp = c.post("https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F")
			.headers(self.headers.clone())
			.header(HeaderName::from_static("referer"), "https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F")
			.header(HeaderName::from_static("origin"), "https://secure.tesco.com")
			.header(HeaderName::from_static("cookie"), self.cookies.as_cookie_header())
			.header(HeaderName::from_static("content-type"), "application/x-www-form-urlencoded")
			.body(body_text)
			.send().await.map_err(|e| format!("{}", e))?;
		self.cookies.apply(&resp);
		if resp.status().is_redirection() {
			// done
			Ok(())
		} else {
			let status = resp.status().as_u16();
			Err(format!("Login API returned status {}", status))
		}
	}
}

impl Checker for TescoChecker {
	fn name(&self) -> &'static str {
		"Tesco"
	}
	fn check<'s>(&'s mut self, c: &'s Client) -> Pin<Box<dyn Future<Output = Result<CheckResult, String>> + 's>> {
		Box::pin(self._check(c))
	}
}
