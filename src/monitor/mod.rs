use chrono::NaiveDate;
use reqwest::Client;
use std::future::Future;
use std::pin::Pin;

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum CheckResult {
	NoSlots,
	SlotsOn(NaiveDate),
	/// Slots available on unknown date
	SlotsAvailable
}

pub trait Checker {
	fn name(&self) -> &'static str;
	fn check<'s>(&'s mut self, c: &'s Client) -> Pin<Box<dyn Future<Output = Result<CheckResult, String>> + 's>>;
}

pub mod amz_fresh;
pub mod waitrose;
pub mod tesco;
pub mod sainsburys;
