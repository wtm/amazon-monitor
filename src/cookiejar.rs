use std::collections::HashMap;
pub struct CookieJar (pub HashMap<String, String>);
use log::{debug, error};
use reqwest::Response;
use reqwest::header::{HeaderName, HeaderMap};

impl CookieJar {
	pub fn new() -> Self {
		Self(HashMap::new())
	}

	pub fn from_cookie_header(init: &str) -> Self {
		let mut hm = HashMap::with_capacity(10);
		for cookie in init.split("; ") {
			let parts: Vec<_> = cookie.splitn(2, '=').collect();
			if parts.len() != 2 {
				error!("Ignoring malformed cookie: {}", cookie);
			} else {
				hm.insert(parts[0].to_owned(), parts[1].to_owned());
			}
		}
		CookieJar(hm)
	}

	pub fn extract_from_headers(mut headers: HeaderMap) -> (Self, HeaderMap) {
		let jar;
		if let Some(val) = headers.get(HeaderName::from_static("cookie")) {
			let c = val.to_str().unwrap();
			jar = Self::from_cookie_header(c);
		} else {
			jar = Self::new();
		}
		while headers.remove(HeaderName::from_static("cookie")).is_some() {}
		(jar, headers)
	}

	pub fn apply(&mut self, resp: &Response) {
		let hs = resp.headers();
		for cookie_str in hs.get_all(HeaderName::from_static("set-cookie")) {
			let cookie_str = match cookie_str.to_str() {
				Ok(s) => s,
				Err(e) => {
					error!("Parsing Set-Cookie: {}", e);
					continue;
				}
			};
			self.apply_inner(cookie_str);
		}
	}

	fn apply_inner(&mut self, cookie_str: &str) {
		let cookie_str = cookie_str.split("; ").next().unwrap();
		let parts: Vec<_> = cookie_str.splitn(2, '=').collect();
		if parts.len() != 2 {
			error!("Malformed Set-Cookie.");
			return;
		}
		debug!("Applying cookie: {} = {}", parts[0], parts[1]);
		self.0.insert(parts[0].to_owned(), parts[1].to_owned());
	}

	pub fn as_cookie_header(&self) -> String {
		let mut header = String::new();
		let mut first = true;
		for (key, val) in self.0.iter() {
			if !first {
				header.push_str("; ");
			}
			header.push_str(key);
			header.push_str("=");
			header.push_str(val);
			first = false;
		}
		header
	}
}

#[test]
fn test() {
	let mut jar = CookieJar::from_cookie_header("a=b; c=d");
	assert_eq!(jar.0.get("a").map(|s| &s[..]), Some("b"));
	assert_eq!(jar.0.get("c").map(|s| &s[..]), Some("d"));
	jar.apply_inner("a=1");
	jar.apply_inner("b=2; HttpOnly");
	assert_eq!(jar.0.get("a").map(|s| &s[..]), Some("1"));
	assert_eq!(jar.0.get("b").map(|s| &s[..]), Some("2"));
}
