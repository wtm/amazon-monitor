mod config;
mod monitor;
mod notify;
mod cookiejar;

const YMD: &str = "%Y-%m-%d";

use reqwest;
use log::{error, info};
use monitor::CheckResult;

static APP_USER_AGENT: &str = concat!(
	env!("CARGO_PKG_NAME"),
	"/",
	env!("CARGO_PKG_VERSION"),
);

#[tokio::main]
async fn main() {
	env_logger::init();

	use std::sync::Arc;

	let conf = match config::config() {
		Ok(c) => c,
		Err(err) => {
			eprintln!("Configuration error: {}", err);
			std::process::exit(1)
		}
	};
	let conf = Arc::new(conf);

	let client = reqwest::Client::builder()
		.redirect(reqwest::redirect::Policy::none())
		.user_agent(APP_USER_AGENT)
		.build().expect("build http client");

	use monitor::Checker;
	let mut checks: Vec<Box<dyn Checker>> = Vec::with_capacity(3);
	if let Some(ref headers) = conf.amz_headers {
		checks.push(Box::new(monitor::amz_fresh::get_checker(headers.clone())));
	}
	if let Some((ref headers, ref body)) = conf.waitrose {
		checks.push(Box::new(monitor::waitrose::get_checker(headers.clone(), body.clone(), conf.waitrose_username.clone().unwrap(), conf.waitrose_password.clone().unwrap())));
	}
	if let Some(ref headers) = conf.tesco_headers {
		checks.push(Box::new(monitor::tesco::get_checker(headers.clone(), conf.tesco_username.clone().unwrap(), conf.tesco_password.clone().unwrap())));
	}
	if let Some(ref headers) = conf.sainsburys_headers {
		checks.push(Box::new(monitor::sainsburys::get_checker(headers.clone())));
	}

	loop {
		if checks.is_empty() {
			error!("Nothing to check.");
			return;
		}

		use futures::future::{join_all, FutureExt};
		let all = join_all(checks.iter_mut().map(|c| {
			let name = c.name();
			c.check(&client).map(move |res| (name, res))
				.map(|(provider, result)| {
					if result.is_err() {
						let errstr = result.as_ref().unwrap_err();
						error!("[{:>15}] Error: {}", provider, errstr);
					} else {
						let res = result.as_ref().unwrap();
						if let CheckResult::SlotsOn(ref day) = res {
							info!("[{:>15}] Slot on {}!", provider, day);
						} else {
							info!("[{:>15}] No slots.", provider);
						}
					}
					(provider, result)
				})
		}));
		for (provider, result) in all.await {
			if result.is_err() {
				let errstr = result.unwrap_err();
				notify::notify_error(&conf, provider, errstr).await;
			} else {
				let res = result.unwrap();
				let cloned_conf = conf.clone();
				tokio::spawn(notify::notify_result(cloned_conf, provider, res));
			}
		}

		tokio::time::delay_for(conf.delay).await;
	}
}
