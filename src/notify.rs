use super::monitor::CheckResult;
use log::{error, debug};
use tokio::time::delay_for;
use std::time::Duration;

use tokio::process::Command;

macro_rules! spawn_proc_and_wait {
	($c:expr,$s:expr) => {
		match $c.spawn() {
			Ok(h) => {
				match h.await {
					Err(e) => {
						error!("Unable to wait on {}: {}", $s, e);
						return false;
					},
					Ok(exit_status) => {
						if !exit_status.success() {
							error!("{} returned error {}", $s, exit_status.code().unwrap_or(-1));
							return false;
						} else {
							debug!("{} exited with code 0.", $s);
						}
					}
				};
			},
			Err(e) => {
				error!("Unable to call {}: {}", $s, e);
				return false;
			}
		};
	};
}

async fn notify_send(msg: String) -> bool {
	let mut c = Command::new("notify-send");
	c.arg("-u");
	c.arg("critical");
	c.arg("--");
	c.arg(msg);
	debug!("exec: notify-send");
	spawn_proc_and_wait!(c, "notify-send");
	true
}

async fn sound_alarm(alarm_sound_file: &std::path::Path) -> bool {
	let mut c = Command::new("ffplay");
	c.arg(alarm_sound_file);
	c.arg("-loop");
	c.arg("2");
	debug!("exec: ffplay");
	spawn_proc_and_wait!(c, "ffplay");
	true
}

pub async fn notify_result<C: AsRef<super::config::Config>>(config: C, provider: &str, result: CheckResult) {
	let config = config.as_ref();
	let day = match result {
		CheckResult::NoSlots => {
			return;
		},
		CheckResult::SlotsOn(day) => Some(day),
		CheckResult::SlotsAvailable => None
	};

	loop {
		let mut failed = false;
		let msg = match day	{
			Some(day) => format!("New {} slot found on {}!", provider, day),
			None => format!("New {} slots available!", provider)
		};
		if !notify_send(msg).await {
			error!("Failed to send notification. Trying again...");
			failed = true;
		}
		if let Some(ref alarm_sound) = config.alarm_sound {
			if !sound_alarm(alarm_sound).await {
				error!("Failed to play sound.");
				failed = true;
			}
		}
		if failed {
			delay_for(Duration::from_secs(1)).await;
			continue;
		}
		break;
	}
}

pub async fn notify_error<C: AsRef<super::config::Config>>(_config: C, provider: &str, errmsg: String) {
	loop {
		if notify_send(format!("{} checker errored: {}", provider, errmsg)).await {
			break;
		}
		error!("Unable to send error notification.");
		delay_for(Duration::from_secs(1)).await;
	}
}
