use std::time::Duration;

use reqwest::header::{HeaderMap, HeaderName, HeaderValue};

pub struct Config {
	pub amz_headers: Option<HeaderMap>,
	pub waitrose: Option<(HeaderMap, crate::monitor::waitrose::QueryParams)>,
	pub waitrose_username: Option<String>,
	pub waitrose_password: Option<String>,
	pub tesco_headers: Option<HeaderMap>,
	pub tesco_username: Option<String>,
	pub tesco_password: Option<String>,
	pub sainsburys_headers: Option<HeaderMap>,
	pub alarm_sound: Option<std::path::PathBuf>,
	pub delay: Duration
}

pub fn config() -> Result<Config, String> {
	use clap::{App, Arg};
	let args = App::new(env!("CARGO_PKG_NAME"))
		.version(env!("CARGO_PKG_VERSION"))
		.arg(Arg::with_name("amz")
					.required(false)
					.takes_value(true)
					.long("amz-headers")
					.help("Amazon headers file"))
		.arg(Arg::with_name("waitrose_header")
					.requires_all(&["waitrose_body", "waitrose_cred"])
					.takes_value(true)
					.long("waitrose-headers")
					.help("Waitrose headers file"))
		.arg(Arg::with_name("waitrose_body")
					.requires_all(&["waitrose_header", "waitrose_cred"])
					.takes_value(true)
					.long("waitrose-query-vars")
					.help("Waitrose query variables (.variables.slotDaysInput object)"))
		.arg(Arg::with_name("waitrose_cred")
					.requires_all(&["waitrose_header", "waitrose_body"])
					.takes_value(true)
					.help("A file with two lines: username and password")
					.long("waitrose-login"))
		.arg(Arg::with_name("tesco")
					.requires_all(&["tesco_cred"])
					.takes_value(true)
					.long("tesco-headers")
					.help("Tesco headers file"))
		.arg(Arg::with_name("tesco_cred")
					.requires_all(&["tesco"])
					.takes_value(true)
					.long("tesco-login")
					.help("A file with two lines: username and password"))
		.arg(Arg::with_name("sainsburys")
					.takes_value(true)
					.long("sainsburys-headers")
					.help("Sainsburys header file"))
		.arg(Arg::with_name("delay")
					.short("d")
					.long("delay")
					.help("Delay between each requests, in secs.")
					.takes_value(true)
					.default_value("20"))
		.arg(Arg::with_name("alarmsound")
					.value_name("alarm-sound")
					.help("Path to a alarm sound file")
					.index(1)
					.required_unless("noalarm")
					.conflicts_with("noalarm"))
		.arg(Arg::with_name("noalarm")
					.help("Disable ffplay alarm")
					.long("no-alarm")
					.required_unless("alarmsound")
					.conflicts_with("alarmsound"))
		.get_matches();

	use std::fs;
	use std::env;

	fn parse_header_file<P: AsRef<std::path::Path>>(path: P) -> Result<HeaderMap, String> {
		let file = fs::File::open(path.as_ref());
		let path_str = path.as_ref().to_string_lossy();
		if let Err(e) = file {
			return Err(format!("Can't open {}: {}", path_str, e));
		}
		use std::io::{BufReader, BufRead};
		let file = BufReader::new(file.unwrap());
		let mut headers = HeaderMap::new();
		for l in file.lines() {
			if let Err(e) = l {
				return Err(format!("Can't read {}: {}", path_str, e));
			}
			let l = l.unwrap();
			let parts: Vec<&str> = l.splitn(2, ": ").collect();
			if parts.len() != 2 {
				return Err(format!("{} is malformed.", path_str));
			}
			use std::str::FromStr;
			headers.insert(match HeaderName::from_str(parts[0]) {
				Ok(h) => h,
				Err(e) => return Err(format!("{} is not a valid header name: {}", &parts[0], e))
			}, HeaderValue::from_str(parts[1]).expect("header value"));
		}
		Ok(headers)
	}

	macro_rules! read_header {
		($param_name:expr) => {{
			let path = args.value_of_os($param_name);
			match path {
				None => None,
				Some(path) => {
					match parse_header_file(path) {
						Ok(headers) => Some(headers),
						Err(e) => return Err(format!("{}: {}", path.to_string_lossy(), e))
					}
				}
			}
		}};
	}

	let delay = {
		let delay = args.value_of("delay").unwrap();
		Duration::from_secs(match delay.parse() {
			Ok(k) => k,
			Err(e) => return Err(format!("{}", e))
		})
	};

	let alarm_sound = {
		if args.is_present("alarmsound") {
			use std::path::PathBuf;
			let path = PathBuf::from(args.value_of_os("alarmsound").unwrap());
			let mtd = fs::metadata(&path);
			if mtd.is_err() || !mtd.unwrap().is_file() {
				return Err(format!("{} is not a valid file.", path.to_string_lossy()));
			}
			Some(path)
		} else {
			None
		}
	};

	macro_rules! read_login {
		($arg_name:expr, $username:ident, $password:ident) => {
			if args.is_present($arg_name) {
				let file = args.value_of_os($arg_name).unwrap();
				let content = fs::read_to_string(&file).map_err(|e| format!("Unable to read {}: {}", file.to_string_lossy(), e))?;
				let mut lines = content.lines();
				$username = Some(lines.next().unwrap().to_owned());
				$password = Some(lines.next().unwrap().to_owned());
				assert_eq!(lines.next(), None);
			} else {
				$username = None;
				$password = None;
			}
		};
	}

	let tesco_username;
	let tesco_password;
	read_login!("tesco_cred", tesco_username, tesco_password);
	let waitrose_username;
	let waitrose_password;
	read_login!("waitrose_cred", waitrose_username, waitrose_password);

	Ok(Config{
		amz_headers: read_header!("amz"),
		waitrose: {
			if let Some(headers) = read_header!("waitrose_header") {
				let p_body = args.value_of_os("waitrose_body").unwrap();
				let body = serde_json::de::from_reader(
					fs::File::open(&p_body)
						.map_err(|e| format!("Can't open {} for read: {}", p_body.to_string_lossy(), e))?)
						.map_err(|e| format!("Can't parse {}: {}", p_body.to_string_lossy(), e))?;
				Some((headers, body))
			} else {
				None
			}
		},
		tesco_headers: read_header!("tesco"),
		tesco_username,
		tesco_password,
		waitrose_username,
		waitrose_password,
		sainsburys_headers: read_header!("sainsburys"),
		delay,
		alarm_sound
	})
}
